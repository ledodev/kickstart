Damit die Kickstart Dateien verarbeitet werden, muss der Speicherort an den Kernel weitergegeben werden. Dazu von einer unveränderten CentOS netinstall ISO starten und beim Auswahlbildschirm "Install CentOS" mit Tab zur vmlinuz Eingabe wechseln.

vmlinuz initrd=initrd.img asknetwork ks=https://bitbucket.org/ledodev/kickstart/raw/master/centos-ks.cfg

![Beispiel](http://www.zerodev.it/images/centos6kickstart1.png)

ODER 

Damit das Kommando automatisiert eingegeben wird muss das ISO angepasst werden. Im Anschluss kann von diesem ISO gebootet werden. Welche Kickstart Datei verarbeitet wird kann im Menü ausgewählt werden.

1. Download CentOS netinstall ISO

    `wget http://ftp.halifax.rwth-aachen.de/centos/7/isos/x86_64/CentOS-7-x86_64-NetInstall-1804.iso`
    
2. ISO mounten

    `mkdir ~/bootiso`
    
    `mount -o loop ~/CentOS-7-x86_64-NetInstall-1804.iso ~/bootiso`
    
3. Arbeitsverzeichnis erstellen und Inhalt von ISO kopieren

    `mkdir ~/bootisocustomized`
    
    `cp -r ~/bootiso/* ~/bootisocustomized/`
    
4. Unmount ISO 

    `umount ~/bootiso && rmdir ~/bootiso`
    
5. Berechtigungen für das Arbeitsverzeichnis

    `chmod -R u+w ~/bootisocustomized`
    
6. Hinzufügen der Menüeinträge zu isolinux.cfg

    ```
        label custom1
         menu label Unattended KS centos-ks
         menu default
         kernel vmlinuz
         append initrd=initrd.img asknetwork ks=https://bitbucket.org/ledodev/kickstart/raw/master/centos-ks.cfg
        label custom2
         menu label Unattended KS centos-server-ks
         kernel vmlinuz
         append initrd=initrd.img asknetwork ks=https://bitbucket.org/ledodev/kickstart/raw/master/centos-server-ks.cfg
        label custom3
         menu label Unattended KS centos-custom-ks
         kernel vmlinuz
         append initrd=initrd.img asknetwork ks=https://bitbucket.org/ledodev/kickstart/raw/master/centos-custom-ks.cfg
    ```

7. Nachdem alle gewünschten Änderungen vorgenommen wurden, kann jetzt das neue ISO erzeugt werden

    `cd ~/bootisocustomized && \ `
    
    `mkisofs -o ~/centos-kickstart.iso -b isolinux.bin -c boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -V "CentOS 7 x86_64" -R -J -v -T isolinux/. .`

8. MD5 Checksume der ISO hinzufügen

    `implantisomd5 ~/centos-kickstart.iso`